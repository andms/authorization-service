package com.taledir.microservices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class LoginUnauthorizedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LoginUnauthorizedException(String arg0) {
		  super(arg0);
    }
    
}