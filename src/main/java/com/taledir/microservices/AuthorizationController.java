package com.taledir.microservices;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
public class AuthorizationController {

	Logger logger = LoggerFactory.getLogger(AuthorizationController.class);
	
	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	AccountRepository accountRepository;

	@PostMapping("/auth/create")
	public ResponseEntity<Map<String, Object>> createAccount(
		@Valid @RequestBody Credentials credentials,
		BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new LoginBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
    }
    
    if(credentials.getUsername() == null || credentials.getEmail() == null || credentials.getPassword() == null) {
      throw new LoginBadRequestException("Username, email, and password must exist for creating account");
    }

		authorizationService.createAccount(credentials);

		String token = authorizationService.getLoginToken(credentials.getUsername());
		return authorizationService.getTokenResponse(token);
	}

	@PatchMapping("/auth/update")
	public ResponseEntity<Map<String, Object>> updatePassword(
		@CookieValue(value = "token", defaultValue = "") String token,
		@Valid @RequestBody PasswordUpdate passwordUpdate,
		BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new LoginBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
    }

		if(token == null || token.length() == 0) {
			throw new InvalidLoginSessionException("Invalid session occurred");
		}

    Map<String, Object> body = authorizationService.updatePassword(token, passwordUpdate);

		return ResponseEntity.ok().body(body);
	}

	@PostMapping("/auth/updateEmail")
	public ResponseEntity<Map<String, Object>> updateEmail(
		@Valid @RequestBody EmailUpdateDto emailUpdateDto,
		BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new LoginBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
    }

		if(emailUpdateDto.getToken() == null || emailUpdateDto.getToken().length() == 0) {
			throw new InvalidLoginSessionException("Invalid session occurred");
		}

    Map<String, Object> body = authorizationService.updateEmail(
      emailUpdateDto.getToken(), emailUpdateDto.getEmail());

		return ResponseEntity.ok().body(body);
	}


	@PostMapping("/auth/login")
	public ResponseEntity<Map<String, Object>> login(
		@CookieValue(value = "token", defaultValue = "") String existingToken,
		@RequestBody Credentials credentials) {

		String username = authorizationService.validateExistingCredentials(credentials);

		if(existingToken != null && existingToken.length() != 0) {
			try {
				logger.warn("Login attempted for user who already has token.  " +
					"Will attempt refresh token first.");
				String refreshToken = authorizationService.getRefreshToken(existingToken);
				return authorizationService.getTokenResponse(refreshToken);
			} catch (LoginBadRequestException e) {
				logger.warn("Refresh attempted instead of login, and refresh failed.  " +
				  "Attempting normal login now.");
			}
		}

		String token = authorizationService.getLoginToken(username);
		return authorizationService.getTokenResponse(token);
	}

	@PostMapping("/auth/refresh")
	public ResponseEntity<Map<String, Object>> refreshToken(
		@CookieValue(value = "token", defaultValue = "") String token) {

		try {
      if(token == null || token.length() == 0) {
        throw new LoginBadRequestException("Invalid session occurred");
      }
			String refreshToken = authorizationService.getRefreshToken(token);
			return authorizationService.getTokenResponse(refreshToken);
		} catch (LoginBadRequestException e) {

      ResponseCookie tokenCookie = authorizationService
        .buildTokenCookie("", 0);

      ResponseCookie isAuthenticatedCookie = authorizationService
        .buildIsAuthenticatedCookie("", 0);
		
			Map<String, Object> errorBody = new HashMap<String, Object>();
      errorBody.put("status", HttpStatus.BAD_REQUEST.value());
      errorBody.put("error", "Bad Request");
			errorBody.put("message", e.getMessage());

			return ResponseEntity.badRequest()
        .header(HttpHeaders.SET_COOKIE, tokenCookie.toString(), isAuthenticatedCookie.toString())
				.body(errorBody);
		}
	}

	@PostMapping("/auth/logout")
	public ResponseEntity<Map<String, Object>> logout(
		@CookieValue(value = "token", defaultValue = "") String token) {

		if(token == null || token.length() == 0) {
			throw new InvalidLoginSessionException("Nothing to do, already logged out");
		}

    ResponseCookie tokenCookie = authorizationService.buildTokenCookie("", 0);
    ResponseCookie isAuthenticatedCookie = authorizationService.buildIsAuthenticatedCookie("", 0);

		//TODO:  if user token is invalid/expired, then failure should not occur here
		//or just a warning in logs
		authorizationService.logout(token);
		
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return ResponseEntity.ok()
      .header(HttpHeaders.SET_COOKIE, tokenCookie.toString(), isAuthenticatedCookie.toString())
			.body(body);
	}

	@PostMapping(
		path = "/auth/introspect",
		consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	public ResponseEntity<Map<String, Object>> tokenInfo(String token) {

		Map<String, Object> body = authorizationService.getTokenInfoBody(token);

		return ResponseEntity.ok().body(body);
	}	

  @PostMapping("/auth/sendPasswordResetEmail")
  public ResponseEntity<Object> sendPasswordResetEmail(@Valid @RequestBody PasswordResetEmailDto passwordResetEmailDto,
      BindingResult bindingResult) {

    if (bindingResult.hasErrors()) {
      if (bindingResult.getFieldError() != null) {
        throw new LoginBadRequestException(bindingResult.getFieldError().getDefaultMessage());
      }
    }

    Account account = accountRepository.findOneByEmail(passwordResetEmailDto.getEmail());

    //if email account exists, send email with token, otherwise do nothing and just return success
		if(account != null) {
      String token = authorizationService.getResetPasswordToken(passwordResetEmailDto.getEmail());

      ResponseEntity<String> response;
      try {
        response = authorizationService.sendPasswordResetEmail(
          passwordResetEmailDto.getEmail(), token);
      } catch(HttpClientErrorException e) {
        Map<String, Object> errorBody = new HashMap<String, Object>();
        errorBody.put("status", HttpStatus.BAD_REQUEST.value());
        errorBody.put("error", "Bad Request");
        errorBody.put("message", e.getResponseBodyAsString());
        return ResponseEntity.badRequest().body(errorBody);
      }

      if(response == null || !response.getStatusCode().equals(HttpStatus.OK) ) {
        throw new LoginBadRequestException("Unable to send reset password email");
      }
    }

    Map<String, Object> body = new HashMap<String, Object>();
    body.put("success", true);

    return ResponseEntity.ok().body(body);
  }

  @PostMapping("/auth/resetPassword")
  public ResponseEntity<Map<String, Object>> resetPassword(
    @Valid @RequestBody ResetPasswordDto resetPasswordDto, BindingResult bindingResult) {

    if (bindingResult.hasErrors()) {
      if (bindingResult.getFieldError() != null) {
        throw new LoginBadRequestException(bindingResult.getFieldError().getDefaultMessage());
      }
    }

    Map<String, Object> body = authorizationService.tokenPasswordReset(resetPasswordDto);

    return ResponseEntity.ok().body(body);
  }

  //TODO:  Throttle login, resetPassword, and sendPasswordResetEmail
  //TODO:  Add recaptcha to create account

}