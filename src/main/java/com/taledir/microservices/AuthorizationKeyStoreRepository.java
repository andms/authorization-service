package com.taledir.microservices;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AuthorizationKeyStoreRepository extends JpaRepository<AuthorizationKeyStore, UUID>{

    Long countByUsername(String username);
    
    List<AuthorizationKeyStore> findAllByUsername(String username);

    @Modifying
    @Transactional
    @Query("delete from AuthorizationKeyStore a where a.username = :username")
    void deleteByUsername(String username);
  
}