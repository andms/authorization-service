package com.taledir.microservices;

import javax.validation.constraints.Pattern;

public class PasswordUpdate {

  private String currentPassword;
  
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,16}$",
		message = "Password must be 8-16 characters, " +
			"contain one lowercase letter, one uppercase letter, one number, " +
			"one special character, and no whitespace characters.")
	private String newPassword;

	public PasswordUpdate(String currentPassword, String newPassword) {
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}