package com.taledir.microservices;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(AuthorizationScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  @Autowired
  PasswordResetRepository passwordResetRepository;

	@Scheduled(fixedRate = 300000)
	public void cleanupPasswordResetsTask() {
    Calendar calendar = Calendar.getInstance();
    
    calendar.add(Calendar.MINUTE, -30);

    Date expiredTime = calendar.getTime();
    String expirationDateTime = dateFormat.format(expiredTime);

    log.info("Starting cleanupPasswordResetsTask to remove anything older than " + expirationDateTime);

    int numberOfDeletes = passwordResetRepository.deleteByCreateTimestampBefore(expiredTime);

    log.info("Successfully removed " + numberOfDeletes + " entries from password_reset table");
	}
}