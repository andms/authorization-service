INSERT INTO account (username, email, password) VALUES
  ('mark-twain', 'mark-twain@example.com', '$2b$12$JlLbJEw4odi1wR3NTjVs.uPDkEAheYs1o5ShKxtaa4wsm91FDV6ki'),
  ('abraham-lincoln', 'abraham-lincoln@example.com', '$2b$12$OczOyooYmOsIpLzN6Iu7deNvOb8CXvHpyDtitWyE1XBP82pMloWom'),
  ('jules-verne', 'jules-verne@example.com', '$2b$12$hTampaDj4JwQ9IZwqlBeaegny6tt8Q6dM6RkZTGiOgXYgIo83Yr7y');
