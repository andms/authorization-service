package com.taledir.microservices;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.equalTo;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
class AuthorizationServiceApplicationTests {

	private static final int EXPIRE_SECONDS = 900;

	@Autowired
	private AuthorizationController authorizationController;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void contextLoads() throws Exception {
		assertThat(authorizationController).isNotNull();
	}

	@Test
    public void Should_ThrowException_When_UsernameDoesNotExist()
        throws Exception {

		Credentials fakeUser = new Credentials("fake-user", "fake-user@example.com", "password1");
		ObjectMapper objectMapper = new ObjectMapper();
		byte[] requestJson =  objectMapper.writeValueAsBytes(fakeUser);

		mockMvc.perform(post("/auth/login")
			.contentType(MediaType.APPLICATION_JSON)
			.content(requestJson))
			.andDo(print())
			.andExpect(status().isUnauthorized());
    }

	@Test
    public void Should_ReturnTokenAndCookie_When_CredentialsAreValid()
        throws Exception {

		Credentials realUser = new Credentials(null, "mark-twain@example.com", "password1");
		ObjectMapper objectMapper = new ObjectMapper();
		byte[] requestJson =  objectMapper.writeValueAsBytes(realUser);

		String token = "token";

		mockMvc.perform(post("/auth/login")
			.contentType(MediaType.APPLICATION_JSON)
			.content(requestJson))
			.andExpect(status().isOk())
			.andExpect(cookie().maxAge(token, EXPIRE_SECONDS))
			.andExpect(cookie().secure(token, true))
			.andExpect(cookie().httpOnly(token, true))
			.andExpect(cookie().path(token, "/"))
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.token_type", is("bearer")))
			.andExpect(jsonPath("$.expires_in", is(EXPIRE_SECONDS)))
			.andExpect(jsonPath("$.access_token", notNullValue()));
    }

	@Test
    public void Should_ReturnNewRefreshToken_When_CredentialsAreValid()
        throws Exception {

		Credentials realUser = new Credentials(null, "mark-twain@example.com", "password1");
		ObjectMapper objectMapper = new ObjectMapper();
		byte[] requestJson =  objectMapper.writeValueAsBytes(realUser);

		ResultActions result = mockMvc.perform(post("/auth/login")
			.contentType(MediaType.APPLICATION_JSON)
			.content(requestJson))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		
		String resultString = result.andReturn().getResponse().getContentAsString();
		JacksonJsonParser jsonParser = new JacksonJsonParser();

		String token = "token";
		String tokenValue = jsonParser.parseMap(resultString).get("access_token").toString();

		Cookie cookie = new Cookie(token, tokenValue);
		cookie.setMaxAge(EXPIRE_SECONDS);
		cookie.setSecure(true);
		cookie.setHttpOnly(true);
		cookie.setPath("/");

		mockMvc.perform(post("/auth/refresh")
			.cookie(cookie))
			.andExpect(status().isOk())
			.andExpect(cookie().maxAge(token, EXPIRE_SECONDS))
			.andExpect(cookie().secure(token, true))
			.andExpect(cookie().httpOnly(token, true))
			.andExpect(cookie().path(token, "/"))
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.token_type", is("bearer")))
			.andExpect(jsonPath("$.expires_in", is(EXPIRE_SECONDS)))
			.andExpect(jsonPath("$.access_token", notNullValue()))
			.andExpect(jsonPath("$.access_token", is(not(equalTo(token)))));
	}
	
	@Test
    public void Should_ReturnException_When_TokenIsModified()
        throws Exception {

		Credentials realUser = new Credentials(null, "mark-twain@example.com", "password1");
		ObjectMapper objectMapper = new ObjectMapper();
		byte[] requestJson =  objectMapper.writeValueAsBytes(realUser);

		ResultActions result = mockMvc.perform(post("/auth/login")
			.contentType(MediaType.APPLICATION_JSON)
			.content(requestJson))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		
		String resultString = result.andReturn().getResponse().getContentAsString();
		JacksonJsonParser jsonParser = new JacksonJsonParser();

		String token = "token";
		String tokenValue = jsonParser.parseMap(resultString).get("access_token").toString();

        StringBuilder modifiedTokenValue = new StringBuilder(tokenValue);
        modifiedTokenValue.setCharAt(100, 'I');

		Cookie cookie = new Cookie(token, modifiedTokenValue.toString());
		cookie.setMaxAge(EXPIRE_SECONDS);
		cookie.setSecure(true);
		cookie.setHttpOnly(true);
		cookie.setPath("/");

		mockMvc.perform(post("/auth/refresh")
			.cookie(cookie))
			.andExpect(status().isBadRequest());
    }	
}
